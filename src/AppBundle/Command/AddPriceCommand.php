<?php

namespace AppBundle\Command;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ActivationCommand
 * @package AppBundle\Command
 */
class AddPriceCommand extends ContainerAwareCommand
{
    /*
     * bin/console snapcart:price:generate 3
     */

    protected function configure()
    {
        $this
            ->setName('snapcart:price:generate')
            ->setDescription('Generate prices.')
            ->addArgument('amount', InputArgument::REQUIRED, 'Amount of elements.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Generating prices',
            '============',
            '',
        ]);

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        $min = -9999999999999999999999;
        $max = 0;
        $timeStartTotal = microtime(true);
        $counter = 1;

        for ($i = 1; $i <= $input->getArgument('amount'); $i++) {
            $timeStart = microtime(true);

            /** @var Product $product */
            $product = new Product();

            $amount = rand(10000, 99999999999999999);
            $product->setPrice(['currency' => 'EUR', 'amount' => $amount]);

            $em->persist($product);
            $em->flush();

            $timeEnd = microtime(true);
            $execTime = floatval($timeEnd - $timeStart) * 1000;
            if ($execTime < $min) {
                $min = $execTime;
            }
            if ($execTime > $max) {
                $max = $execTime;
            }

            $output->writeln(
                sprintf(
                    '    %s. Value %s has been added. Executed in %s milliseconds. Memory: %s',
                    $counter,
                    str_pad($amount, 20),
                    str_pad(round($execTime, 4), 20),
                    str_pad($this->getMemoryUsage(), 10)
                )
            );

            $counter++;

            unset($product);
            unset($timeStart);
            unset($execTime);
            unset($timeEnd);
        }

        $timeEndTotal = microtime(true);

        // retrieve the argument value using getArgument()
        $output->writeln(sprintf('Min time: %s milliseconds', $min));
        $output->writeln(sprintf('Max time: %s milliseconds.', $max));
        $output->writeln(sprintf('Total Execution time: %s seconds.', floatval($timeEndTotal - $timeStartTotal)));
        $output->writeln(sprintf('Memory pick: %s.', $this->getMemoryUsage(true)));
    }

    protected function getMemoryUsage($pick = false)
    {
        $mem_usage = $pick ? memory_get_peak_usage(true) : memory_get_usage(true);

        if ($mem_usage < 1024) {
            $m = $mem_usage . " b";
        } elseif ($mem_usage < 1048576) {
            $m = "Kb " . round($mem_usage / 1024, 2);
        } else {
            $m = "Mb " . round($mem_usage / 1048576, 2);
        }

        return $m;
    }
}
