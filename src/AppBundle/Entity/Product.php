<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var array
     *
     * @ORM\Column(name="price", type="json_array", nullable=true)
     */
    protected $price;

    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param array $data
     *
     * @return Product
     */
    public function setPrice(array $data = [])
    {
        if ($data != null) {
            $this->price = $data;
        }

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
}
