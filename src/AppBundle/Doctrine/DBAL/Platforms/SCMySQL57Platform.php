<?php

namespace AppBundle\Doctrine\DBAL\Platforms;

use Doctrine\DBAL\Platforms\MySQL57Platform;

/**
 * Class SCMySQL57Platform
 * @package AppBundle\Doctrine\DBAL\Platforms
 */
class SCMySQL57Platform extends MySQL57Platform
{
    /**
     * {@inheritdoc}
     */
    public function getJsonTypeDeclarationSQL(array $field)
    {
        return 'JSON';
    }

    /**
     * {@inheritdoc}
     */
    public function hasNativeJsonType()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
        parent::initializeDoctrineTypeMappings();
        $this->doctrineTypeMapping['json'] = 'json_array';
    }
}
